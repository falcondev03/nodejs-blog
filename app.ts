import express from 'express'
// import express = require('express')
import bodyParser = require('body-parser')
import indexRouter = require('./routes') 
import {Sequelize} from 'sequelize'

const sequelize:Sequelize = new Sequelize('blog_nodejs','renegarcia', 'Dayli030317#',{
	host:'localhost',
	dialect: 'mysql'
})

sequelize
	.authenticate()
	.then(() => console.log("Conectado"))

const app:express.Application = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended : true}))
app.use('/static', express.static('public'))
app.use('/files', express.static('files'))
app.set('view engine', 'pug')
app.use('/', <express.Router>indexRouter)

app.listen(3000, () => {
	console.log('el servidor esta funcionando')
})